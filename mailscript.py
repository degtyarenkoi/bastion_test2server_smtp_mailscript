import paramiko
import sys, time, re
from optparse import OptionParser


def init():
    """
       optparse() method
       returns a dictionary of parameters, inputted by user
   """
    parser = OptionParser(usage="usage: %prog [options] host username password port")
    parser.add_option("-r", "--remote",
                      action="store",
                      dest="hostname",
                      default="prod-ops-smtp5.vega.ironport.com",
                      help="Input remote hostname")
    parser.add_option("-u", "--user",
                      action="store",
                      dest="username",
                      help="Input username")
    parser.add_option("-p", "--password",
                      action="store",
                      dest="password",
                      help="Input password")
    parser.add_option("-g", "--gateway",
                      action="store",
                      dest="gateway",
                      default="22",
                      type='int',
                      help="Input gateway(port)")
    parser.add_option("-e", "--email",
                      action="store",
                      dest="email",
                      help="Input an email")

    options, args = parser.parse_args()

    if not options.username:
        parser.error("Input username")
    elif not options.password:
        parser.error("Input password")
    elif not options.email:
        parser.error("Input an email")
    else:
        return options


class LdapConnect:
    def __init__(self, hostname, username, password, port, email):
        self.hostname = hostname
        self.password = password
        self.username = username
        self.port = port
        self.email = email

    def get_connection_instance(self):
        """
           get_connection_instance() method
           takes set of arguments to connect to SMTP server
           returns an ssh connection instance
       """
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=self.hostname, username=self.username, password=self.password, port=self.port)

        return client

    def ldaptest(self):
        """
           ldaptest() method
           takes set of arguments to connect to SMTP server
           returns ldaptest result (0 - OK, 2 - CRITICAL, 3 - UNKNOWN)
       """

        client = self.get_connection_instance()
        channel = client.invoke_shell()
        channel_data = str()

        while True:
            if channel.recv_ready():
                time.sleep(3)
                channel.send('ldaptest\n')
                time.sleep(0.1)
                channel_data += channel.recv(9999)
                if channel_data.endswith('[1]> '):
                    channel.send('1\n')
                    time.sleep(0.1)
                    channel_data += channel.recv(9999)
                if channel_data.endswith('[]> '):
                    channel.send('%s\n' % self.email)
                    time.sleep(2)
                    channel_data += channel.recv(9999)
                return channel_data
            else:
                continue

        client.close()

    def ldapconfig(self):
        """
           ldapconfig() method
           takes set of arguments to connect to SMTP server
           returns ldapconfig result (0 - OK, 2 - CRITICAL, 3 - UNKNOWN)
       """
        client = self.get_connection_instance()

        channel = client.invoke_shell()
        channel_data = str()
        configs_amount = list()
        configs_states = dict()

        while True:
            if channel.recv_ready():
                time.sleep(3)
                channel.send('ldapconfig\n')
                time.sleep(0.1)
                channel_data += channel.recv(9999)
                if channel_data.endswith('[1]> '):
                    channel.send('1\n')
                    time.sleep(0.1)
                    channel_data += channel.recv(9999)
                regexp_result = re.findall("configurations:(.*?)Choose", channel_data, re.DOTALL)
                for i in regexp_result:
                    configs_amount.append(i)
                i = 1
                if len(configs_amount) > 0:
                    while i <= len(configs_amount):
                        channel.send('edit\n')
                        time.sleep(0.1)
                        channel_data += channel.recv(9999)
                        if channel_data.endswith('[]> '):
                            channel.send('%r\n' % i)
                            time.sleep(0.1)
                            channel_data += channel.recv(9999)
                            if channel_data.endswith('[]> '):
                                channel.send('test\n')
                                time.sleep(0.1)
                                channel_data += channel.recv(9999)
                                if "Result:succeeded" in channel_data:
                                    configs_states[configs_amount[i-1]] = 'succeeded'
                                else:
                                    configs_states[configs_amount[i-1]] = 'failed'
                        i += 1
                    return configs_states
                else:
                    print "CRITICAL: There isnt any config"
                    sys.exit(2)
            else:
                continue

        client.close()


    def main(self):
        """
           main() method
           implements main logic of ldaptest/ldapconfig tests
       """
        try:
            ldaptest_response = self.ldaptest()
            if "Action: pass" in ldaptest_response or "Reason: no matching LDAP record was found" in ldaptest_response:
                print "OK: Success"
                sys.exit(0)
            elif "Error: Connection Error" in ldaptest_response:
                ldapconfig_response = self.ldapconfig()
        except Exception as e:
            print "Unknown, exception is %s" % e
            sys.exit(3)


if __name__ == "__main__":
    opt = init()
    ldap_connect = LdapConnect(opt.hostname, opt.username, opt.password, opt.gateway, opt.email)
    ldap_connect.main()

